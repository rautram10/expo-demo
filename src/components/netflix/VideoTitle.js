import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import SimpleText from '../../common/SimpleText';

const VideoTitle = props => {
  return (
    <View style={styles.title}>
      <SimpleText text={props.title} size={14} />
      <View style={{width: 15}} />
      <Icon name="angle-right" size={17} color="#000000" />
    </View>
  );
};

export default VideoTitle;

const styles = StyleSheet.create({
  title: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
