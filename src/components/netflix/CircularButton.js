import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const CircularButton = props => {
  return (
    <View style={styles.circle}>
      <Icon name="play-arrow" size={20} coloe="black" />
    </View>
  );
};

export default CircularButton;

const styles = StyleSheet.create({
  circle: {
    height: 30,
    width: 30,
    borderRadius: 15,
    borderWidth: 3,
    borderColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
