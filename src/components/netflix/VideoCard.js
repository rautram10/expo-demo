import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  NativeModules,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import CircularButton from './CircularButton';
import SimpleText from '../../common/SimpleText';
import ResultLine from '../../common/ResultLine';
import {
  TapGestureHandler,
  State,
  PanGestureHandler,
} from 'react-native-gesture-handler';
import {SharedView} from '../../nativemodule/VideoView';

class VideoCard extends React.Component {
  onSingleTap = event => {
    if (event.nativeEvent.state === State.ACTIVE) {
      const data = event.nativeEvent;
      console.log(data);
    }
  };

  startTransition = () => {
    NativeModules.ActivityStarter.startTransition('');
  };
  render() {
    return (
      <View style={{flex: 1}}>
        <SharedView style={{flex: 1}} id="hello">
          <Image source={{uri: 'dog'}} style={{height: 100, width: 100}} />
        </SharedView>
        <Button title="Click" onPress={this.startTransition} />
      </View>
    );
  }
}

export default VideoCard;

const styles = StyleSheet.create({
  card: {
    borderColor: '#fafafa',
    backgroundColor: '#fafafa',
    elevation: 3,
    shadowColor: '#000000',
    shadowOpacity: 0.3,
    borderRadius: 20,
    marginRight: 10,
    // backgroundColor: 'rgba(217,230,226, 0.4)',
  },
  image: {
    width: 170,
    height: 190,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  bottom: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  right: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  left: {
    justifyContent: 'center',
  },
});
