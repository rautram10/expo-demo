import React from 'react';
import {requireNativeComponent} from 'react-native';

const BottomSheet = props => {
  return <Sheet {...props} style={props.style} />;
};
const Sheet = requireNativeComponent('BottomSheet');

export default BottomSheet;
