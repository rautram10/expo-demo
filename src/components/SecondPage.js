import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import VideoView from '../nativemodule/VideoView';

class SecondPage extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <View style={{flex: 1}}>
        <VideoView style={{flex: 1}} />
      </View>
    );
  }
}

export default SecondPage;

const styles = StyleSheet.create({});
