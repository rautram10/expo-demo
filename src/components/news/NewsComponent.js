import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import SimpleText from '../../common/SimpleText';

class NewsComponent extends React.Component {
  render() {
    return (
      <View style={{flex: 1, borderRadius: 5}}>
        <Image
          source={{uri: 'dog'}}
          style={{
            height: 300,
            width: '100%',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
          }}
        />

        <View style={{flex: 1, padding: 15, backgroundColor: 'white'}}>
          <SimpleText text={this.props.index} bold />
          <SimpleText
            text="World Health organizarion declares corona virus outbreak a pamdemic"
            size={16}
            color={'#000000'}
          />
          <View style={{height: 10}} />
          <View style={{flex: 1}}>
            <Text style={styles.paragraph}>
              lorem epsum jkbsjkbc bcjkshc jkncsdkjhsdc bjkschbcd jkbnsjkadc
              ,msnjksdcn nbskjbcnsd jnbsjkbcds jkbnkdjcsncd jkhnscjknhcd lorem
              epsum jkbsjkbc bcjkshc jkncsdkjhsdc bjkschbcd jkbnsjkadc
              ,msnjksdcn nbskjbcnsd jnbsjkbcds jkbnkdjcsncd jkhnscjknhcd lorem
              epsum jkbsjkbc bcjkshc jkncsdkjhsdc bjkschbcd jkbnsjkadc
              ,msnjksdcn nbskjbcnsd jnbsjkbcds jkbnkdjcsncd jkhnscjknhcd
            </Text>
            <View style={{height: 10}} />
            <SimpleText
              text="Swipe Up get More news"
              size={12}
              color="gray"
              color="#f86b17"
            />
            <View style={{height: 10}} />
            <SimpleText
              text="Swipe Down to get Previous news"
              size={12}
              color="#f86b17"
            />
          </View>
        </View>
        <View style={styles.bottom}>
          <View style={{flex: 1, justifyContent: 'center', padding: 15}}>
            <SimpleText text="Random News" color="white" size={14} />
          </View>
        </View>
      </View>
    );
  }
}

export default NewsComponent;

const styles = StyleSheet.create({
  paragraph: {
    fontFamily: 'Roboto',
    lineHeight: 22,
    fontSize: 16,
    color: '#666',
    textAlign: 'justify',
  },
  bottom: {
    position: 'absolute',
    height: 60,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.9)',
  },
});
