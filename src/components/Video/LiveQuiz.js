import React from 'react';
import {Text, View, StyleSheet, Dimensions, Button} from 'react-native';
import VideoView from '../../nativemodule/VideoView';
import AnimatedCircularProgress from '../circular/AnimatedCircularProgress';
import PercentageCircle from '../circular/PercentageCircle';

const {height, width} = Dimensions.get('window');

class LiveQuiz extends React.Component {
  constructor() {
    super();
    this.state = {
      visible: true,
    };
  }

  toggle = () => {
    this.setState({
      visible: !this.state.visible,
    });
  };
  render() {
    return (
      <View style={{flex: 1}}>
        {this.state.visible ? (
          <VideoView src="" style={{height: height, width: width}} />
        ) : (
          <PercentageCircle radius={35} percent={50} color={'#3498db'}>
            <VideoView src="" style={{height: 90, width: 90}} />
          </PercentageCircle>
        )}

        <View style={styles.button}>
          <Button title="Toggle" onPress={() => this.toggle()} />
        </View>
      </View>
    );
  }
}

export default LiveQuiz;

const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    left: 15,
    right: 15,
    bottom: 15,
  },
});
