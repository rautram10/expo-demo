import React from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import Animated, {
  set,
  lessThan,
  greaterThan,
  greaterOrEq,
} from 'react-native-reanimated';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import SimpleButton from '../common/SimpleButton';
import NewsComponent from '../components/news/NewsComponent';
import {runTiming} from '../common/config/runTiming';

const {height, width} = Dimensions.get('screen');

const data = [];

for (i = 1; i < 30; i++) {
  data.push(i);
}

const {
  Value,
  cond,
  event,
  add,
  eq,
  call,
  stopClock,
  Clock,
  lessOrEq,
  sub,
} = Animated;

class InShorts extends React.Component {
  constructor(props) {
    super(props);
    this.clock = new Clock();
    this.clock2 = new Clock();
    this.offset = new Value(0);
    this.upOffset = new Value(-height);
    this.translateY = new Value(0);
    this.upTranslateY = new Value(0);
    this.gestureState = new Value(-1);
    this._onGestureEvent = event([
      {
        nativeEvent: {
          translationY: this.translateY,
          state: this.gestureState,
        },
      },
    ]);
    this.addY = cond(
      lessThan(this.translateY, 0),
      add(this.offset, this.translateY),
      0,
    );
    this.addUpY = cond(
      greaterThan(this.translateY, 0),
      add(this.translateY, this.upOffset),
      -height,
    );
    this.moveY = cond(
      eq(this.gestureState, State.ACTIVE),
      [stopClock(this.clock), this.addY],
      [
        cond(
          lessOrEq(this.translateY, -width / 2),
          [runTiming(this.clock, this.addY, -height, 300)],
          [runTiming(this.clock, this.addY, 0, 300)],
        ),
      ],
    );

    this.down = cond(
      eq(this.gestureState, State.ACTIVE),
      [stopClock(this.clock2), this.addUpY],
      [
        cond(
          lessOrEq(this.translateY, height / 3),
          [runTiming(this.clock2, this.addUpY, -height, 300)],
          [runTiming(this.clock2, this.addUpY, 0, 300)],
        ),
      ],
    );

    this.state = {
      currentIndex: 1,
    };
  }

  onDrop = ([x, y]) => {
    if (x > 0 && x >= height / 3) {
      setTimeout(() => {
        this.props.decrementIndex();
      }, 300);
    }
    if (x < 0 && x <= -width / 2) {
      setTimeout(() => {
        this.props.incrementIndex();
      }, 300);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Animated.Code>
          {() =>
            cond(
              eq(this.gestureState, State.END),
              call([this.translateY, this.addUpY], this.onDrop),
            )
          }
        </Animated.Code>
        <View style={{flex: 1}}>
          <PanGestureHandler
            {...this.props}
            onGestureEvent={this._onGestureEvent}
            onHandlerStateChange={this._onGestureEvent}>
            <Animated.View
              style={{
                flex: 1,
                transform: [
                  {
                    translateY: this.moveY,
                  },
                ],
              }}>
              <NewsComponent index={this.props.currentIndex} />
            </Animated.View>
          </PanGestureHandler>
        </View>
        {this.props.currentIndex > 1 && (
          <View style={styles.absolute}>
            <Animated.View
              style={{
                flex: 1,
                transform: [
                  {
                    translateY: this.down,
                  },
                ],
              }}>
              <NewsComponent index={this.props.currentIndex - 1} />
            </Animated.View>
          </View>
        )}
      </View>
    );
  }
}

export default InShorts;

const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    height: '100%',
    width: width,
  },
  container: {
    flex: 1,
    borderRadius: 10,
  },
});
