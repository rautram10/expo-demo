import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  NativeModules,
} from 'react-native';
import AnimatedOption from '../common/AnimatedOptionView';
import SimpleText from '../common/SimpleText';

class FirstScreen extends React.Component {
  openLive = () => {
    NativeModules.ActivityStarter.startReactActivity('second');
  };
  openList = () => {
    NativeModules.ActivityStarter.startPlayList();
  };

  openNative = () => {
    NativeModules.ActivityStarter.startActivity();
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: 15,
          backgroundColor: '#ffffff',
        }}>
        <TouchableOpacity style={styles.buttom} onPress={this.openLive}>
          <SimpleText text="Live Video" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttom} onPress={this.openList}>
          <SimpleText text="PlayList Example" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttom} onPress={this.openNative}>
          <SimpleText text="Pure Native Activity" />
        </TouchableOpacity>
      </View>
    );
  }
}

export default FirstScreen;

const styles = StyleSheet.create({
  buttom: {
    padding: 15,
    alignItems: 'center',
    marginVertical: 20,
    borderColor: '#fafafa',
    borderWidth: 0.5,
    elevation: 3,
    backgroundColor: '#fafafa',
    borderRadius: 30,
  },
});
