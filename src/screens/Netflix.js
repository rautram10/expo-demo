import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Dimensions,
  TouchableHighlight,
  YellowBox,
} from 'react-native';
import Animated, {Easing} from 'react-native-reanimated';
import VideoCard from '../components/netflix/VideoCard';
import VideoTitle from '../components/netflix/VideoTitle';
import FastImage from 'react-native-fast-image';
import {TapGestureHandler} from 'react-native-gesture-handler';

YellowBox.ignoreWarnings(['Warning: ReactNative.createElement']);

const {
  createAnimatedComponent,
  Value,
  interpolate,
  Extrapolate,
  set,
} = Animated;

const {height} = Dimensions.get('window');

const AnimatedImage = createAnimatedComponent(FastImage);

class Netflix extends React.Component {
  constructor() {
    super();
    this.height = new Value(190);
    this.width = interpolate(this.height, {
      inputRange: [170, 300],
      outputRange: [190, 320],
      extrapolate: Extrapolate.CLAMP,
    });
    this.opacity = interpolate(this.height, {
      inputRange: [190, 191, 300],
      outputRange: [0, 1, 1],
    });
    this.state = {
      show: false,
    };
  }

  animate = () => {
    Animated.timing(this.height, {
      toValue: 300,
      duration: 300,
      easing: Easing.ease,
      useNativeDriver: true,
    }).start();
  };

  showAnim = () => {
    this.setState(
      {
        show: true,
      },
      () => {
        this.animate();
      },
    );
  };
  reverseAnim = () => {
    Animated.timing(this.height, {
      toValue: 190,
      duration: 300,
      easing: Easing.ease,
      useNativeDriver: true,
    }).start(() => {
      this.setState({show: false});
    });
  };
  render() {
    const data = [1, 2, 3];
    return (
      <View style={{flex: 1, padding: 15}}>
        <VideoCard style={{flex: 1}} />
        {/* <VideoTitle title="Continue watching" />
        <View style={{height: 10}} />
        <FlatList
          data={data}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            return (
              <View>
                <VideoCard />
              </View>
            );
          }}
          keyExtractor={item => item.toString()}
        />

        {this.state.show && (
          <View style={styles.absolute}>
            <Text onPress={this.reverseAnim}>Back</Text>
            <Animated.View style={{paddingLeft: 15, paddingTop: 30}}>
              <AnimatedImage
                source={{
                  uri:
                    'https://previews.123rf.com/images/gvardy/gvardy1807/gvardy180700140/105540062-couple-of-husky-dogs-kissing-on-sand-seaside-with-sea-and-sky-background.jpg',
                }}
                style={[
                  styles.image,
                  {
                    height: this.height,
                    width: this.width,
                  },
                ]}
              />
            </Animated.View>
          </View>
        )} */}
      </View>
    );
  }
}

export default Netflix;

const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    height: height,
    backgroundColor: '#ffffff',
  },
  image: {
    borderRadius: 20,
  },
});
