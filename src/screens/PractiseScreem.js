import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Animated, {Easing} from 'react-native-reanimated';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import SimpleText from '../common/SimpleText';

const {Value, eq, cond, Clock, stopClock, event, add, set} = Animated;

const data = [];

for (i = 1; i < 20; i++) {
  data.push(i);
}

class PractiseScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        {data.map(item => {
          return (
            <View style={styles.absolute} key={item}>
              <View style={{flex: 1}}>
                <Box item={item} />
              </View>
            </View>
          );
        })}
      </View>
    );
  }
}

export default PractiseScreen;

class Box extends React.Component {
  constructor(props) {
    super(props);
    this.offset = new Value(0);
    this.offsetY = new Value(0);
    this.translateX = new Value(0);
    this.translateY = new Value(0);
    this.gestureState = new Value(-1);
    this._onGestureEvent = event([
      {
        nativeEvent: {
          translationX: this.translateX,
          state: this.gestureState,
          translationY: this.translateY,
        },
      },
    ]);
    this.addX = add(this.offset, this.translateX);
    this.addY = add(this.offsetY, this.translateY);
    this.moveX = cond(
      eq(this.gestureState, State.ACTIVE),
      [this.addX],
      [set(this.offset, this.addX), this.offset],
    );
    this.moveY = cond(
      eq(this.gestureState, State.ACTIVE),
      [this.addY],
      [set(this.offsetY, this.addY), this.offsetY],
    );
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <Animated.View
          style={{
            height: 100,
            width: 100,
            transform: [
              {
                translateX: this.moveX,
                translateY: this.moveY,
              },
            ],
          }}>
          <PanGestureHandler
            {...this.props}
            onGestureEvent={this._onGestureEvent}
            onHandlerStateChange={this._onGestureEvent}>
            <Animated.View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'tomato',
                elevation: 3,
              }}>
              <SimpleText
                text={this.props.item}
                bold
                size={20}
                color={'#fff'}
              />
            </Animated.View>
          </PanGestureHandler>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    left: 15,
    top: 15,
    height: 100,
    width: 100,
  },
});
