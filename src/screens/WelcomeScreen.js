import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Button,
  findNodeHandle,
  UIManager,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import {ExoCustomView} from '../nativemodule/VideoView';
import mediaJSON from '../quiz/data';

const {height, width} = Dimensions.get('screen');

class WelcomeScreen extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      data: [],
      play: false,
    };
  }
  async componentDidMount() {
    const data = [];

    mediaJSON.categories[0].videos.map(item => {
      data.push(item.sources[0]);
    });

    this.setState({
      data,
    });
  }

  openVideo = number => {
    this.video = findNodeHandle(this._videoView);
    UIManager.dispatchViewManagerCommand(this.video, 0, [number]);
  };

  render() {
    return (
      <View style={{flex: 1}}>
        {this.state.data.length > 0 && (
          <ExoCustomView
            ref={ref => (this._videoView = ref)}
            data={this.state.data}
            play={this.state.play}
            style={{height: 200, width: '100%'}}
            onChange={evt => {
              const data = evt.nativeEvent;
              console.log(data);
            }}
          />
        )}

        <FlatList
          data={mediaJSON.categories[0].videos}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                style={styles.item}
                onPress={() => this.openVideo(index)}>
                <Text>{item.title}</Text>
              </TouchableOpacity>
            );
          }}
          keyExtractor={item => item.title}
        />
      </View>
    );
  }
}

export default WelcomeScreen;

const styles = StyleSheet.create({
  outer: {
    height: 240,
    width: 240,
    borderRadius: 120,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
  inner: {
    height: 230,
    width: 230,
    borderRadius: 115,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    height: 50,
    marginHorizontal: 15,
    justifyContent: 'center',
    borderBottomColor: '#9a9a9a',
    borderBottomWidth: 0.5,
  },
});
