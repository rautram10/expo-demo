import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import {SharedView} from '../nativemodule/VideoView';

const SimpleSheet = () => {
  return (
    <View style={{flex: 1}}>
      <SharedView style={{flex: 1}} id="hello">
        <Image source={{uri: 'dog'}} style={{height: 400, width: 400}} />
      </SharedView>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  title: {
    fontSize: 40,
    color: '#ffffff',
  },
  bottomSheet: {
    backgroundColor: '#202122',
    alignItems: 'center',
  },
  sheetTitle: {
    marginTop: 16,
    color: '#fff',
    fontSize: 26,
  },
});

export default SimpleSheet;
