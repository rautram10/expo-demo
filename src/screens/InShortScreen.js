import React from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import InShorts from './InShorts';
import Animated from 'react-native-reanimated';
import NewsComponent from '../components/news/NewsComponent';
import SimpleText from '../common/SimpleText';
import {RectButton} from 'react-native-gesture-handler';

const data = [];

for (i = 1; i < 30; i++) {
  data.push(i);
}

const {height, width} = Dimensions.get('window');

class InShortScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 1,
    };
  }

  incrementIndex = () => {
    if (this.state.currentIndex !== 30) {
      this.setState({
        currentIndex: this.state.currentIndex + 1,
      });
    }
  };

  decrementIndex = () => {
    if (this.state.currentIndex !== 1) {
      this.setState({
        currentIndex: this.state.currentIndex - 1,
      });
    }
  };
  reload = () => {
    this.setState({
      currentIndex: 1,
    });
  };
  render() {
    return (
      <View style={{flex: 1}}>
        {data
          .map(item => {
            if (this.state.currentIndex == item) {
              return (
                <View
                  style={{
                    position: 'absolute',
                    height: height - 24,
                    width,
                    zIndex: 999,
                    elevation: 5,
                  }}>
                  <InShorts
                    incrementIndex={this.incrementIndex.bind(this)}
                    decrementIndex={this.decrementIndex.bind(this)}
                    currentIndex={this.state.currentIndex}
                    key={item}
                  />
                </View>
              );
            } else if (item < this.state.currentIndex) {
              return null;
            } else {
              return (
                <Animated.View
                  style={{
                    position: 'absolute',
                    height: height - 24,
                    width,
                    zIndex: 999,
                    elevation: 5,
                  }}>
                  <View style={{flex: 1}}>
                    <NewsComponent index={item} />
                  </View>
                </Animated.View>
              );
            }
          })
          .reverse()}
        <View style={styles.main}>
          <View style={{alignItems: 'center'}}>
            <SimpleText
              text="Thank you for reading all our posts"
              color={'#f86b17'}
            />
          </View>
          <RectButton style={styles.button} onPress={this.reload}>
            <SimpleText text="Reload" color={'#ffffff'} bold />
          </RectButton>
        </View>
      </View>
    );
  }
}

export default InShortScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    padding: 15,
  },
  button: {
    marginTop: 20,
    borderWidth: 0.5,
    backgroundColor: '#f86b17',
    padding: 15,
    alignItems: 'center',
    borderRadius: 40,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    elevation: 3,
  },
});
