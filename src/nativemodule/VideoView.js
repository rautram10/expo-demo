import {requireNativeComponent} from 'react-native';

const VideoView = requireNativeComponent('VideoView');

export default VideoView;

export const ExoCustomView = requireNativeComponent('ExoCustomView');

export const SharedView = requireNativeComponent('SharedElement');
