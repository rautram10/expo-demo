import React from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import Animated, {Easing, set, lessOrEq} from 'react-native-reanimated';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import {runTiming} from './config/runTiming';
import NewsComponent from '../components/news/NewsComponent';
import {runSpring} from './config/runSpring';

const {
  Value,
  event,
  add,
  sub,
  cond,
  eq,
  Clock,
  stopClock,
  greaterOrEq,
  call,
} = Animated;

const {height, width} = Dimensions.get('screen');

class SimpleButton extends React.Component {
  constructor() {
    super();
    this.clock = new Clock();
    this.gestureState = new Value(-1);
    this.offset = new Value(0);
    this.translateY = new Value(0);
    this._onGestureEvent = event([
      {
        nativeEvent: {
          translationY: this.translateY,
          state: this.gestureState,
        },
      },
    ]);

    this.addY = add(this.offset, this.translateY);

    this.marginLeft = cond(
      eq(this.gestureState, State.ACTIVE),
      [stopClock(this.clock), this.addY],
      [
        cond(
          lessOrEq(this.translateY, -width / 2),
          [runTiming(this.clock, this.translateY, -height, 300)],
          [runTiming(this.clock, this.translateY, 0, 300)],
        ),
      ],
    );
    this.state = {
      currentIndex: 1,
    };
  }

  onDrop = ([y]) => {
    if (y < -width / 2) {
    }
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <Animated.Code>
          {() =>
            cond(
              eq(this.gestureState, State.END),
              call([this.translateY], this.onDrop),
            )
          }
        </Animated.Code>

        <PanGestureHandler
          {...this.props}
          onGestureEvent={this._onGestureEvent}
          onHandlerStateChange={this._onGestureEvent}>
          <Animated.View
            style={[
              styles.box,
              {
                transform: [
                  {
                    translateY: this.marginLeft,
                  },
                ],
              },
            ]}>
            <View style={{flex: 1}}>
              <NewsComponent index={this.props.item} />
            </View>
          </Animated.View>
        </PanGestureHandler>
      </View>
    );
  }
}

export default SimpleButton;

const styles = StyleSheet.create({
  box: {
    height: '100%',
    width: width,
    elevation: 3,
  },
});
