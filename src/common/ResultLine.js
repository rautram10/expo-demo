import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const ResultLine = props => {
  return (
    <View style={[styles.line]}>
      <View style={[styles.result, {width: props.width}]} />
    </View>
  );
};

export default ResultLine;

const styles = StyleSheet.create({
  line: {
    height: 5,
    backgroundColor: 'gray',
  },
  result: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 5,
    backgroundColor: 'tomato',
  },
});
