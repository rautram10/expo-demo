import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  InteractionManager,
} from 'react-native';
import Animated, {Easing} from 'react-native-reanimated';
import {runTiming} from './config/runTiming';
import SimpleText from './SimpleText';

const {width} = Dimensions.get('window');

const {Value, Clock} = Animated;

class AnimatedOption extends React.Component {
  constructor(props) {
    super(props);
    this.clock = new Clock();
    this.width = new Value(0);
    this.animWidth = runTiming(
      this.clock,
      0,
      ((width - 60) / props.full) * props.value,
      (2000 * props.value) / props.full,
      1000,
    );
  }
  componentDidMount() {
    // InteractionManager.runAfterInteractions(() => {
    //   setTimeout(() => {
    //     this.props.hideQuestion();
    //   }, 10000);
    // });
  }
  render() {
    return (
      <View style={styles.box}>
        <View style={styles.absolute}>
          <SimpleText text="Hello" color={'#000000'} />
        </View>
        <Animated.View
          style={{
            height: 50,
            width: this.animWidth,
            backgroundColor: 'rgba(0,0,0,0.2)',
            borderRadius: 40,
          }}
        />
      </View>
    );
  }
}

export default AnimatedOption;

const styles = StyleSheet.create({
  box: {
    height: 50,
    borderWidth: 0,
    borderRadius: 40,
    elevation: 3,
    backgroundColor: 'white',
    width: width - 60,
    marginLeft: 30,
    marginTop: 15,
  },
  absolute: {
    position: 'absolute',
    top: 15,
    left: 15,
  },
});
