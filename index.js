/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React from 'react';
import {gestureHandlerRootHOC} from 'react-native-gesture-handler';
import App from './App';
import {name as appName} from './app.json';
import SecondScreen from './src/screens/SecondScreen';

import {Provider} from 'react-redux';

import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import reducers from './src/reducers';
import WelcomeScreen from './src/screens/WelcomeScreen';
import Netflix from './src/screens/Netflix';
import SimpleSheet from './src/screens/SimpleSheet';
import InShorts from './src/screens/InShorts';
import InShortScreen from './src/screens/InShortScreen';
import CustomVideo from './src/components/Video/CustomVideo';
import FirstScreen from './src/screens/FirstScreen';
import PractiseScreen from './src/screens/PractiseScreem';

const middleware = [thunk, logger];

const store = createStore(reducers, applyMiddleware(...middleware));

console.disableYellowBox = true;

function ReduxProvider(Component) {
  return props => (
    <Provider store={store}>
      <Component {...props} />
    </Provider>
  );
}

AppRegistry.registerComponent('example', () =>
  ReduxProvider(gestureHandlerRootHOC(InShortScreen)),
);

AppRegistry.registerComponent('second', () => CustomVideo);

AppRegistry.registerComponent('third', () =>
  ReduxProvider(gestureHandlerRootHOC(WelcomeScreen)),
);
