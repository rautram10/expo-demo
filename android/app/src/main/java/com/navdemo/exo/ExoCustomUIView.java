package com.navdemo.exo;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Outline;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.navdemo.R;

import java.util.ArrayList;
import java.util.List;


public class ExoCustomUIView extends FrameLayout {

    private PlaybackStateListener playbackStateListener;
    ThemedReactContext reactContext;
    SimpleExoPlayer player;
    PlayerView playerView;
    private boolean playWhenReady = true;
    private int currentWindow = 0;
    private long playbackPosition = 0;
    private boolean mExoPlayerFullscreen = false;
    private ImageView mFullScreenIcon;
    private ImageView pauseView, backButton, rewindButton, forwardButton;
    private Dialog mFullScreenDialog;
    private boolean isPlaying = true;

    private float height = 300;

    AppCompatActivity appCompatActivity;


    String url = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4";

    public ExoCustomUIView(@NonNull ThemedReactContext context) {

        super(context);
        reactContext = context;
        appCompatActivity = (AppCompatActivity) context.getCurrentActivity();
        //   initializeView();
        reactContext.addLifecycleEventListener(new LifecycleEventListener() {
            @Override
            public void onHostResume() {
                System.out.println("I am Called");
                System.out.println("I am Called");
                Toast.makeText(reactContext, "Its Resumed", Toast.LENGTH_LONG).show();
                // initializeView(url);

            }

            @Override
            public void onHostPause() {
                releasePlayer();

            }

            @Override
            public void onHostDestroy() {
                reactContext.removeLifecycleEventListener(this);
                releasePlayer();
            }

        });
    }

    public void setHeight(float h)
    {
        height = h;
    }

    public void initializeView(ReadableArray data) {

        List<String> videos = new ArrayList<>();
        for(int i=0;i<data.size(); i++)
        {
            videos.add(data.getString(i));
        }
        // url = source;
        inflate(reactContext, R.layout.custom_exo, this);
        playerView = (PlayerView) findViewById(R.id.player);
        playbackStateListener = new PlaybackStateListener();
        pauseView = findViewById(R.id.pause);
        backButton =  findViewById(R.id.back);
        rewindButton = findViewById(R.id.rewind);
        forwardButton = findViewById(R.id.forward);
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.seekTo(player.getCurrentPosition() + 10000);
            }
        });
        rewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.seekTo(player.getCurrentPosition()-10000);
            }
        });

        backButton.setVisibility(View.INVISIBLE);

        pauseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPlaying) {
                    pauseView.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_arrow_black_24dp));
                    player.setPlayWhenReady(false);
                    isPlaying = false;
                }
                else {
                    isPlaying = true;
                    pauseView.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_black_24dp));
                    player.setPlayWhenReady(true);
                }
            }
        });

        mFullScreenIcon = (ImageView) findViewById(R.id.full_screen);
        mFullScreenIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreen();
            }
        });

        if (player == null) {
            DefaultTrackSelector trackSelector = new DefaultTrackSelector();
            trackSelector.setParameters(
                    trackSelector.buildUponParameters().setMaxVideoSizeSd());
            player = ExoPlayerFactory.newSimpleInstance(reactContext, trackSelector);
        }
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        playerView.setPlayer(player);
        // Uri uri = Uri.parse(source);
        //   MediaSource mediaSource = buildMediaSource(uri);
        ConcatenatingMediaSource sources = createPlayList(videos);
        player.setPlayWhenReady(playWhenReady);
        player.seekTo(currentWindow, playbackPosition);
        player.addListener(playbackStateListener);
        player.prepare(sources, false, false);

        player.addListener(new SimpleExoPlayer.EventListener() {

            @Override
            public void onPositionDiscontinuity(int reason) {
                int lastWindowIndex = player.getCurrentWindowIndex();
                System.out.println("the current window index is "+lastWindowIndex);
                WritableMap map = Arguments.createMap();
                map.putInt("index", lastWindowIndex);

                ReactContext context = (ReactContext) getContext();
                context.getJSModule(RCTEventEmitter.class).receiveEvent(getId(), "topChange", map);

            }
        });


    }

    private MediaSource buildMediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(reactContext, "gundruk");
        //  DashMediaSource.Factory mediaSourceFactory = new DashMediaSource.Factory(dataSourceFactory);
        MediaSource mediaSourceFactory = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
        //  return mediaSourceFactory.createMediaSource(uri);
        return mediaSourceFactory;
    }

    private void openFullScreen() {
        if(!mExoPlayerFullscreen) {

            mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(appCompatActivity, R.drawable.ic_fullscreen_skrink));
            appCompatActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
                    |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            mExoPlayerFullscreen = true;
            appCompatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) playerView.getLayoutParams();
            params.width = params.MATCH_PARENT;
            params.height = params.MATCH_PARENT;
            playerView.setLayoutParams(params);
        }
        else {
            WritableMap map = Arguments.createMap();
            map.putString("name", "hello");
            ReactContext context = (ReactContext) getContext();
            mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(appCompatActivity, R.drawable.ic_fullscreen_expand));
            appCompatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mExoPlayerFullscreen = false;
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) playerView.getLayoutParams();
            params.width = params.MATCH_PARENT;
            params.height = params.MATCH_PARENT;
            playerView.setLayoutParams(params);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

        }
    }

    private ConcatenatingMediaSource createPlayList(List<String> uri)
    {
        MediaSource[] sources = new MediaSource[uri.size()];

        for(int i=0; i < uri.size(); i++)
        {
            sources[i] = buildMediaSource(Uri.parse(uri.get(i)));
        }

        ConcatenatingMediaSource mediaSource = new ConcatenatingMediaSource(sources);

        return mediaSource;

    }



    private void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.removeListener(playbackStateListener);
            player.release();
            player = null;
        }
    }

    public void playSpecific(int number)
    {
        if(player != null)
        {
            pauseView.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_black_24dp));
            player.seekTo(number, C.TIME_UNSET);
            //  playerView.setLayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
            playerView.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setRoundRect(0,0,view.getWidth(), view.getHeight(), 0);
                }
            });

            playerView.setClipToOutline(true);
            player.setPlayWhenReady(true);
        }
    }



    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private class PlaybackStateListener implements SimpleExoPlayer.EventListener{

        @Override
        public void onPlayerStateChanged(boolean playWhenReady,
                                         int playbackState) {
            String stateString;
            switch (playbackState) {
                case ExoPlayer.STATE_IDLE:
                    stateString = "ExoPlayer.STATE_IDLE      -";
                    break;
                case ExoPlayer.STATE_BUFFERING:
                    stateString = "ExoPlayer.STATE_BUFFERING -";

                    break;
                case ExoPlayer.STATE_READY:
                    stateString = "ExoPlayer.STATE_READY     -";
                    break;
                case ExoPlayer.STATE_ENDED:
                    stateString = "ExoPlayer.STATE_ENDED     -";
                    break;
                default:
                    stateString = "UNKNOWN_STATE             -";
                    break;
            }
            Log.d("Demo", "changed state to " + stateString
                    + " playWhenReady: " + playWhenReady);
        }
    }



}
