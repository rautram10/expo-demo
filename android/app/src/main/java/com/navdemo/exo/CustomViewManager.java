package com.navdemo.exo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.navdemo.VideoView;

import java.util.HashMap;
import java.util.Map;

public class CustomViewManager extends SimpleViewManager<ExoCustomUIView> {
    private static final int COMMAND_FAST = 0;
    private static final int COMMAND_REWIND = 1;
    private static final int RELEASE_PLAYER = 2;
    @NonNull
    @Override
    public String getName() {
        return "ExoCustomView";
    }

    @NonNull
    @Override
    protected ExoCustomUIView createViewInstance(@NonNull ThemedReactContext reactContext) {
        ExoCustomUIView customUIView = new ExoCustomUIView(reactContext);
        return customUIView;
    }

    @ReactProp(name = "data")
    public void setSource(ExoCustomUIView uiView, ReadableArray data)
    {
        uiView.initializeView(data);
    }

    @ReactProp(name = "height")
    public void setHeight(ExoCustomUIView uiView, float height)
    {
        uiView.setHeight(height);
    }


    @Override
    public void receiveCommand(@NonNull final ExoCustomUIView root, int commandId, @Nullable final ReadableArray args) {
        switch (commandId)
        {
            case COMMAND_FAST:
                System.out.println("the arguments are "+args.getInt(0));
                int number = args.getInt(0);
                root.playSpecific(number);
                // root.fastForward();
                break;
            case COMMAND_REWIND:
                root.playSpecific(0);
                //  root.rewind();
                break;
            case RELEASE_PLAYER:
                root.playSpecific(0);
                //  root.releasePlayer();
                break;
            default:
                root.playSpecific(0);
                // root.releasePlayer();
        }
    }

    @Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        Map<String, Integer> map = this.CreateMap(
                "forward", COMMAND_FAST,
                "rewind", COMMAND_REWIND,
                "release", RELEASE_PLAYER
        );

        return map;
    }

    public static <K, V> Map<K, V> CreateMap(
            K k1, V v1, K k2, V v2, K k3, V v3) {
        Map map = new HashMap<K, V>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        return map;
    }
}
