package com.navdemo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Outline;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.uimanager.ThemedReactContext;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSourceFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;


public class VideoView extends FrameLayout    {
    ThemedReactContext mcontext;
    SimpleExoPlayer player;
    private static PlayerView playerView;


    public VideoView(@NonNull ThemedReactContext context) {
        super(context);
        mcontext = context;

       // initilizeView();
        context.addLifecycleEventListener(new LifecycleEventListener() {
            @Override
            public void onHostResume() {

                Log.d("exo", "The player is "+player);
            }

            @Override
            public void onHostPause() {

                releasePlayer();
            }

            @Override
            public void onHostDestroy() {

                releasePlayer();
                context.removeLifecycleEventListener(this);
            }

        });
    }

    public void initilizeView(String src) {

        inflate(mcontext, R.layout.activity_video, this);

        playerView = findViewById(R.id.video_view);

       // BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

       // TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);

        if(player == null) {

            DefaultTrackSelector trackSelector = new DefaultTrackSelector();
            trackSelector.setParameters(
                    trackSelector.buildUponParameters().setMaxVideoSizeSd());
            player = ExoPlayerFactory.newSimpleInstance(mcontext, trackSelector);

//            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//
//            player = ExoPlayerFactory.newSimpleInstance(mcontext, trackSelector);

            playerView.setPlayer(player);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
            player.setPlayWhenReady(true);

            RtmpDataSourceFactory rtmpDataSourceFactory = new RtmpDataSourceFactory();



            MediaSource videoSource = new ExtractorMediaSource.Factory(rtmpDataSourceFactory)
                    .createMediaSource(Uri.parse(src));

            player.prepare(videoSource, false, false);

        }




    }

    public void fastForward()
    {
        playerView.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRoundRect(0,0,view.getWidth(), view.getHeight(), view.getHeight()/2);
            }
        });

        playerView.setClipToOutline(true);

    }

    public void rewind()
    {
        playerView.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setRoundRect(0,0,view.getWidth(), view.getHeight(), 0);
            }
        });

        playerView.setClipToOutline(true);
    }

    public void releasePlayer()
    {
        if(player != null) {

            player.release();
            player = null;
        }
        else {
            System.out.println("Player is null");
        }

    }





}
