package com.navdemo.views;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.views.view.ReactViewGroup;
import com.navdemo.R;

public class SharedViewManager extends ViewGroupManager<ReactViewGroup> {


    @NonNull
    @Override
    public String getName() {
        return "SharedElement";
    }

    @NonNull
    @Override
    protected ReactViewGroup createViewInstance(@NonNull ThemedReactContext reactContext) {
        return new ReactViewGroup(reactContext);
    }

    @Override
    public void addView(ReactViewGroup parent, View child, int index) {
        String transitionName = (String) parent.getTag(R.id.react_shared_element_transition_name);
        ViewCompat.setTransitionName(child, transitionName);
        child.setId(R.id.view_id);
        child.setTransitionName("abc");
        String name = child.getTransitionName();
        System.out.println("the name is "+name);
      //  System.out.println("the transition name from view manager is "+transitionName);
        parent.addView(child, index);
    }

    @ReactProp(name ="id")
    public void setIdentifier(ReactViewGroup reactViewGroup, String id)
    {
        reactViewGroup.setTag(R.id.react_shared_element_transition_name, id);
    }
}
