package com.navdemo.modules;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.core.view.ViewCompat;
import androidx.transition.TransitionSet;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Fade;
import androidx.transition.TransitionInflater;


import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.navdemo.R;
import com.navdemo.ThirdActivity;
import com.navdemo.Video;
import com.navdemo.fragment.ReactFragment;
import com.navdemo.fragment.SecondActivity;


public class ActivityStarter extends ReactContextBaseJavaModule {

     Context context;
     private static final long MOVE_DEFAULT_TIME = 1000;
     private static final long FADE_DEFAULT_TIME = 300;
     AppCompatActivity activity;

     ReactFragment reactFragment;

    public ActivityStarter(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;


    }

    @NonNull
    @Override
    public String getName() {
        return "ActivityStarter";
    }

    @ReactMethod
    public void startActivity()
    {

        Intent intent = new Intent(activity, Video.class);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        });
    }

    @ReactMethod
    public void startReactActivity(String name)
    {
        Intent intent;
        activity = (AppCompatActivity) getCurrentActivity();
        if (name == "second") {
            intent = new Intent(activity, SecondActivity.class);
        }
        else {
            intent = new Intent(activity, ThirdActivity.class);
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        });
    }

    @ReactMethod
    public void startPlayList()
    {
        activity = (AppCompatActivity) getCurrentActivity();
      Intent  intent = new Intent(activity, SecondActivity.class);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        });
    }

    @ReactMethod
    public void startTransition(String nativeID) {
        activity = (AppCompatActivity) getCurrentActivity();
        reactFragment = new ReactFragment.Builder("hello").build();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reactFragment.setSharedElementEnterTransition(new DetailsTransition());
                reactFragment.setSharedElementReturnTransition(new DetailsTransition());
                Fade enterFade = new Fade();
                enterFade.setDuration(MOVE_DEFAULT_TIME + FADE_DEFAULT_TIME);
                reactFragment.setEnterTransition(enterFade);
                reactFragment.setExitTransition(new Fade());
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                ReactFragment firstFragment = (ReactFragment) fragmentManager.findFragmentById(R.id.container_main);
                firstFragment.setExitTransition(new Fade());
                View view = activity.findViewById(R.id.view_id);
                FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.addSharedElement(view, ViewCompat.getTransitionName(view));
                fragmentTransaction.replace(R.id.container_main, reactFragment, reactFragment.getClass().getSimpleName());
                fragmentTransaction.addToBackStack(reactFragment.getClass().getSimpleName());
                fragmentTransaction.commit();

            }
        });


    }
}
